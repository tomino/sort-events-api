# Project Title

A small Flask project for processing events.

The events are structured (poepleID, posX, posY). Once there's more than one event for a given peopleID, the events are sorted/completed.

## Application run
To run the application just run the run.py script. Then open your web broswer and submit this adress: http://localhost:5000/. All you need to do now, is to submit events in the form. The submited events are automatically processed and corresponding tables are updated.

### Prerequisites

See requirements.txt


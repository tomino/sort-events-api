from flask import Flask, jsonify, request, abort
from flask import make_response, render_template
from sorteventsapi.forms import EventForm
from sorteventsapi.events import Events
from sorteventsapi import app


# create Events object for storing and processing events
events = Events()


@app.route('/')
def home():
    """
    Landing page. Here the user can add new events and see the results.
    :return:
    """
    error = None
    form = EventForm(request.form)
    return render_template('sortevents.html', form=form, events=events, error=error)


@app.route('/add_event/', methods=['POST'])
def add_event():
    """
    Adding new event to the "database" of events represented by Events object.
    :return:
    """
    error = None
    form = EventForm(request.form)

    if form.validate_on_submit():
        event = (form.peopleID.data, form.posX.data, form.posY.data)
        events.insert(event)
        # print('all: {}'.format(events.events))
        # print('open: {}'.format(events.open_events))
        # print('closed: {}'.format(events.closed_events))

    return render_template('sortevents.html', form=form, events=events, error=error)

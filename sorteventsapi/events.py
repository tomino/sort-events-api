from collections import defaultdict

class Events():
    """Class for processing events.

    Attributes:
        events (list): List of all submitted events.
        open_events (dict): Dictionary of open events, i.e. the start position.
        closed_events (list): List of all closed events, i.e. events with both the start and the end positions.
    """

    def __init__(self):
        self.events = list()  # List of all events: [(peopleID, posX, posY), ...]
        self.open_events = dict()  # Dict of open events: {peopleID: (posX_start, posY_start)}
        # self.closed_events = defaultdict(list)  # {peopleID: [(posX_start, posY_start, posX_end, posY_end), ...}
        self.closed_events = list()  # [(peopleID, posX_start, posY_start, posX_end, posY_end), ...]

    def insert(self, event):
        """
        Inserts event. If there's an open event for current peopleID - close it, otherwise insert it as an open event.
        :param event: (peopleID, posX, posY)
        :return:
        """
        # append event to the list of all events
        self.events.append(event)
        peopleID, posX, posY = event

        # If there's an open event for peopleID, close it.
        try:
            posX_open, posY_open = self.open_events[peopleID]
            # self.closed_events[peopleID].append((posX_open, posY_open, posX, posY))
            self.closed_events.append((peopleID, posX_open, posY_open, posX, posY))
            self.open_events[peopleID] = (posX, posY)
            print('Added a new event: {}, paired with: {}'.format(event, (peopleID, posX_open, posY_open)))
        # Otherwise create a new open event.
        except KeyError:
            self.open_events[peopleID] = (posY, posY)
            print('Added a new open event: {}'.format(event))

from flask_wtf import Form
from wtforms import IntegerField
from wtforms.validators import DataRequired


class EventForm(Form):
    peopleID = IntegerField('peopleID', validators=[DataRequired()])
    posX = IntegerField('posX', validators=[DataRequired()])
    posY = IntegerField('posY', validators=[DataRequired()])